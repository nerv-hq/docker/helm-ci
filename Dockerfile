FROM alpine:3.19

RUN apk add --no-cache \
    git \
    helm

RUN helm plugin install https://github.com/chartmuseum/helm-push

ENTRYPOINT ["helm"]
